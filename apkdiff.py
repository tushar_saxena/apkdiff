#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Add docstring here

__author__ = 'aDd1kTeD2Ka0s'
__email__ = 'add1kted2ka0s@gmail.com'
"""

# Imports ---------------------------------------------------------------------
import argparse
import subprocess
import os
import sys

# ---------------------------------
class ColorPrinter:
	# -----------------------------
	"""
	Usage:
	cprint = ColorPrinter()
	cprint.cfg('c','m','bux').out('Hello','World!')
	cprint.rst().out('Bye now...')
	See: http://stackoverflow.com/a/21786287/472610
	See: https://en.wikipedia.org/wiki/ANSI_escape_code
	"""

	COLCODE = {
		'k': 0,  # black
		'r': 1,  # red
		'g': 2,  # green
		'y': 3,  # yellow
		'b': 4,  # blue
		'm': 5,  # magenta
		'c': 6,  # cyan
		'w': 7  # white
	}

	FMTCODE = {
		'b': 1,  # bold
		'f': 2,  # faint
		'i': 3,  # italic
		'u': 4,  # underline
		'x': 5,  # blinking
		'y': 6,  # fast blinking
		'r': 7,  # reverse
		'h': 8,  # hide
		's': 9,  # strikethrough
	}

	# ---------------------------------
	def __init__(self):
		# -----------------------------
		self.rst()

	# ---------------------------------
	def rst(self):
		# -----------------------------
		self.prop = {
			'st': [],
			'fg': None,
			'bg': None
		}
		return self

	# ---------------------------------
	def cfg(self, fg, bg=None, st=None):
		# -----------------------------
		return self.rst().st(st).fg(fg).bg(bg)

	# ---------------------------------
	def st(self, st):
		# -----------------------------
		if isinstance(st, int):
			assert (st >= 0) and (st < 10), 'Style should be in {0 .. 9}.'
			self.prop['st'].append(st)
		elif isinstance(st, basestring):
			for s in st:
				self.st(self.FMTCODE[s])
		return self

	# ---------------------------------
	def fg(self, fg):
		# -----------------------------
		if isinstance(fg, int):
			assert (fg >= 0) and (fg < 8), 'Color should be in {0 .. 7}.'
			self.prop['fg'] = 30 + fg
		elif isinstance(fg, basestring):
			self.fg(self.COLCODE[fg])
		return self

	# ---------------------------------
	def bg(self, bg):
		# -----------------------------
		if isinstance(bg, int):
			assert (bg >= 0) and (bg < 8), 'Color should be in {0 .. 7}.'
			self.prop['bg'] = 40 + bg
		elif isinstance(bg, basestring):
			self.bg(self.COLCODE[bg])
		return self

	# ---------------------------------
	def fmt(self, *args):
		# -----------------------------

		# accept multiple inputs, and concatenate them with spaces
		s = " ".join(map(str, args))

		# get anything that is not None
		w = self.prop['st'] + [self.prop['fg'], self.prop['bg']]
		w = [str(x) for x in w if x is not None]

		# return formatted string
		return '\x1b[%sm%s\x1b[0m' % (';'.join(w), s) if w else s

	# ---------------------------------
	def out(self, *args):
		# -----------------------------
		print self.fmt(*args)

# ---------------------------------
class Apkdiff:
	# -----------------------------
	"""
	Add docstring here
	"""

	# ---------------------------------
	def __init__(self, apk1, apk2):
		# ---------------------------------
		"""
		Add docstring here
		"""
		self.cprint = ColorPrinter()
		self.parsed_dictionary = dict()

		self.parse_unzip_output(input_list=['unzip', '-vl', apk1], key=1)
		self.parse_unzip_output(input_list=['unzip', '-vl', apk2], key=2)

		self.compare_outputs()

	# ---------------------------------
	def parse_unzip_output(self, input_list, key):
		# -----------------------------
		"""
		Add docstring here
		"""
		# TODO: File validations
		# TODO: Capture exceptions
		# TODO: Filename contains spaces?
		# Execute unzip process and capture output
		process = subprocess.Popen(input_list, stdout=subprocess.PIPE)
		output = process.stdout.readlines()
		# Loop through output
		for line in output[3:-2]:
			split_line = line[:-1].split()
			filename = split_line[7]
			if filename not in self.parsed_dictionary:
				self.parsed_dictionary[filename] = dict()
			self.parsed_dictionary[filename][key] = dict()
			self.parsed_dictionary[filename][key] = {
				'uncompressed_size': int(split_line[0]),
				'mode': split_line[1],
				'compressed_size': int(split_line[2]),
				'ratio': int(split_line[3].split('%')[0]),
				'timestamp': split_line[4] + ' ' + split_line[5],
				'crc32': split_line[6]
			}

	# ---------------------------------
	def compare_outputs(self):
		# -----------------------------
		"""
		Add docstring here
		"""
		# TODO: Support 3 modes - all, crc, filesize
		# TODO: Convert timestamp to date

		# Init
		metrics = {
			'files': {
				'added': 0,
				'removed': 0,
				'modified': 0,
			},
			'size': {
				'overall': {
					1: {
						'uncompressed': 0,
						'compressed': 0
					},
					2: {
						'uncompressed': 0,
						'compressed': 0
					}
				},
				'added': {
					1: {
						'uncompressed': 0,
						'compressed': 0
					},
					2: {
						'uncompressed': 0,
						'compressed': 0
					}
				},
				'removed': {
					1: {
						'uncompressed': 0,
						'compressed': 0
					},
					2: {
						'uncompressed': 0,
						'compressed': 0
					}
				},
				'modified': {
					1: {
						'uncompressed': 0,
						'compressed': 0
					},
					2: {
						'uncompressed': 0,
						'compressed': 0
					}
				}
			}
		}

		# Detailed Log
		print self.cprint.cfg('w', 'k', 'bu').fmt('Detailed Log')
		print

		for key in sorted(self.parsed_dictionary.keys()):
			metrics['size']['overall'][1]['uncompressed'] += self.parsed_dictionary[key][1]['uncompressed_size'] if 1 in self.parsed_dictionary[key] else 0
			metrics['size']['overall'][1]['compressed'] += self.parsed_dictionary[key][1]['compressed_size'] if 1 in self.parsed_dictionary[key] else 0
			metrics['size']['overall'][2]['uncompressed'] += self.parsed_dictionary[key][2]['uncompressed_size'] if 2 in self.parsed_dictionary[key] else 0
			metrics['size']['overall'][2]['compressed'] += self.parsed_dictionary[key][2]['compressed_size'] if 2 in self.parsed_dictionary[key] else 0

			if 1 not in self.parsed_dictionary[key]:
				metrics['files']['added'] += 1
				metrics['size']['added'][2]['uncompressed'] += int(self.parsed_dictionary[key][2]['uncompressed_size'])
				metrics['size']['added'][2]['compressed'] += int(self.parsed_dictionary[key][2]['compressed_size'])
				print self.cprint.cfg('g', 'k', 'b').fmt('+ %s' % key)
				print '  ➛ CRC-32            : %s' % self.parsed_dictionary[key][2]['crc32']
				print '  ➛ Timestamp         : %s' % self.parsed_dictionary[key][2]['timestamp']
				print '  ➛ Uncompressed Size : %s' % self.humanize(self.parsed_dictionary[key][2]['uncompressed_size'])
				print '  ➛ Compressed Size   : %s' % self.humanize(self.parsed_dictionary[key][2]['compressed_size'])
			elif 2 not in self.parsed_dictionary[key]:
				metrics['files']['removed'] += 1
				metrics['size']['removed'][1]['uncompressed'] += int(self.parsed_dictionary[key][1]['uncompressed_size'])
				metrics['size']['removed'][1]['compressed'] += int(self.parsed_dictionary[key][1]['compressed_size'])
				print self.cprint.cfg('r', 'k', 'b').fmt('- %s' % key)
				print '  ➛ CRC-32            : %s' % self.parsed_dictionary[key][1]['crc32']
				print '  ➛ Timestamp         : %s' % self.parsed_dictionary[key][1]['timestamp']
				print '  ➛ Uncompressed Size : %s' % self.humanize(self.parsed_dictionary[key][1]['uncompressed_size'])
				print '  ➛ Compressed Size   : %s' % self.humanize(self.parsed_dictionary[key][1]['compressed_size'])
			elif self.parsed_dictionary[key][1]['crc32'] != self.parsed_dictionary[key][2]['crc32']:
				metrics['files']['modified'] += 1
				metrics['size']['modified'][1]['uncompressed'] += int(self.parsed_dictionary[key][1]['uncompressed_size'])
				metrics['size']['modified'][1]['compressed'] += int(self.parsed_dictionary[key][1]['compressed_size'])
				metrics['size']['modified'][2]['uncompressed'] += int(self.parsed_dictionary[key][2]['uncompressed_size'])
				metrics['size']['modified'][2]['compressed'] += int(self.parsed_dictionary[key][2]['compressed_size'])
				print self.cprint.cfg('y', 'k', 'b').fmt('# %s' % key)
				print '  ➛ CRC-32            : %s | %s' % (self.parsed_dictionary[key][1]['crc32'], self.parsed_dictionary[key][2]['crc32'])
				print '  ➛ Timestamp         : %s | %s' % (self.parsed_dictionary[key][1]['timestamp'], self.parsed_dictionary[key][2]['timestamp'])
				print '  ➛ Uncompressed Size : %s | %s' % (self.humanize(self.parsed_dictionary[key][1]['uncompressed_size']), self.humanize(self.parsed_dictionary[key][2]['uncompressed_size']))
				print '  ➛ Compressed Size   : %s | %s' % (self.humanize(self.parsed_dictionary[key][1]['compressed_size']), self.humanize(self.parsed_dictionary[key][2]['compressed_size']))

		# Summary
		print
		print self.cprint.cfg('w', 'k', 'bu').fmt('Summary')
		print
		print '* Number of Files'
		print '  ➛ Added    : %d' % metrics['files']['added']
		print '  ➛ Removed  : %d' % metrics['files']['removed']
		print '  ➛ Modified : %d' % metrics['files']['modified']
		print
		print '* File Size'
		print '  ➛ Overall APK'
		print '    ➛ Uncompressed Size : %s | %s (%.2f%%)' % (self.humanize(metrics['size']['overall'][1]['uncompressed']), self.humanize(metrics['size']['overall'][2]['uncompressed']), ((metrics['size']['overall'][2]['uncompressed'] - metrics['size']['overall'][1]['uncompressed']) * 100 / metrics['size']['overall'][2]['uncompressed']))
		print '    ➛ Compressed Size   : %s | %s (%.2f%%)' % (self.humanize(metrics['size']['overall'][1]['compressed']), self.humanize(metrics['size']['overall'][2]['compressed']), ((metrics['size']['overall'][2]['compressed'] - metrics['size']['overall'][1]['compressed']) * 100 / metrics['size']['overall'][2]['compressed']))
		print '  ➛ Added File Size'
		print '    ➛ Uncompressed Size : %s' % self.humanize(metrics['size']['added'][2]['uncompressed'])
		print '    ➛ Compressed Size   : %s' % self.humanize(metrics['size']['added'][2]['compressed'])
		print '  ➛ Removed File Size'
		print '    ➛ Uncompressed Size : %s' % self.humanize(metrics['size']['removed'][1]['uncompressed'])
		print '    ➛ Compressed Size   : %s' % self.humanize(metrics['size']['removed'][1]['compressed'])
		print '  ➛ Modified File Size'
		print '    ➛ Uncompressed Size : %s | %s (%.2f%%)' % (self.humanize(metrics['size']['modified'][1]['uncompressed']), self.humanize(metrics['size']['modified'][2]['uncompressed']), ((metrics['size']['modified'][2]['uncompressed'] - metrics['size']['modified'][1]['uncompressed']) * 100 / metrics['size']['modified'][2]['uncompressed']))
		print '    ➛ Compressed Size   : %s | %s (%.2f%%)' % (self.humanize(metrics['size']['modified'][1]['compressed']), self.humanize(metrics['size']['modified'][2]['compressed']), ((metrics['size']['modified'][2]['compressed'] - metrics['size']['modified'][1]['compressed']) * 100 / metrics['size']['modified'][2]['compressed']))

	# ---------------------------------
	def humanize(self, num, spaces=1, suffix='B'):
		# -----------------------------
		for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
			if abs(num) < 1024.0:
				return "%3.3f%s%s%s" % (num, ' ' * spaces, unit, suffix)
			num /= 1024.0
		return "%.3f%s%s%s" % (num, ' ' * spaces, 'Y', suffix)

# Main ------------------------------------------------------------------------
if __name__ == '__main__':
	# Argument parser
	parser = argparse.ArgumentParser(description='Find the difference between 2 APKs')
	parser.add_argument('apk', metavar='apk', type=str, nargs='+', help='An APK to compare')
	args = parser.parse_args()

	# Validate arguments
	# --- Number of arguments
	if len(args.apk) != 2:
		print 'Too many! You can compare exactly 2 APKs. Exiting ...'
		sys.exit(1)
	# --- File exist?
	for i in range(2):
		if not os.path.exists(args.apk[i]):
			print 'File <%s> does not exist! Exiting ...' % args.apk[i]
			sys.exit(2)

	# Execute APK Diff
	apkdiff = Apkdiff(args.apk[0], args.apk[1])
