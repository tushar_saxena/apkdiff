# Introduction

This project allows you to compare 2 different APKs and get the difference between them. Useful when managing iterative releases. 

# Running

    $ python apkdiff.py [apk1] [apk2]